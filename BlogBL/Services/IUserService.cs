﻿using BlogDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBL.Services
{
    public interface IUserService
    {
        Task<IEnumerable<BlogUser>> GetAllUsersAsync();
        BlogUser GetUserByUserId(int userId);
        Task<BlogUser> GetUserByUserName(string username);
        Task<BlogUser> CreateUserAsync(BlogUser user);
        Task<bool> DeleteAsync(int id);
    }
}
