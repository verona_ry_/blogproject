﻿using BlogDAL.Models;
using BlogDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBL.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public async Task<BlogComments> CreateAsync(BlogComments comment)
        {
            return await _commentRepository.CreateAsync(comment);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _commentRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<BlogComments>> GetAllAsync()
        {
            return await _commentRepository.GetAllAsync();
        }

        public IEnumerable<BlogComments> GetAllByArticleIdAsync(int id)
        {
            return _commentRepository.GetAllByArticleIdAsync(id);
        }

        public async Task<BlogComments> GetByIdAsync(int id)
        {
            return await _commentRepository.GetByIdAsync(id);
        }

        public async Task<bool> UpdateAsync(BlogComments comment)
        {
            return await _commentRepository.UpdateAsync(comment);
        }
    }
}
