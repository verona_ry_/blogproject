﻿using BlogDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBL.Services
{
    public interface ICommentService
    {
        Task<BlogComments> CreateAsync(BlogComments comment);
        Task<IEnumerable<BlogComments>> GetAllAsync();
        IEnumerable<BlogComments> GetAllByArticleIdAsync(int id);
        Task<BlogComments> GetByIdAsync(int id);
        Task<bool> UpdateAsync(BlogComments comment);
        Task<bool> DeleteAsync(int id);
    }
}
