﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogBL.Services
{
    public interface ITagService
    {
        Task<IEnumerable<Tag>> CreateAsync(string tagsString);
        Task<IEnumerable<Tag>> GetAllAsync();
        IEnumerable<Tag> GetAllByArticleId(int articleId);
        Task<Tag> GetByIdAsync(int id);
        Task<bool> DeleteAsync(int id);
    }
}
