﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Repositories;
using BlogDAL.Models;

namespace BlogBL.Services
{
    public class TagService : ITagService
    {
        private readonly ITagsRepository _tagsRepository;

        public TagService(ITagsRepository tagsRepository)
        {
            _tagsRepository = tagsRepository;
        }

        public async Task<IEnumerable<Tag>> CreateAsync(string tagsString)
        {
            return await _tagsRepository.CreateTagListAsync(tagsString);

        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _tagsRepository.DeleteAsync(id);
        }

        
        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await _tagsRepository.GetAllAsync();
        }

        public IEnumerable<Tag> GetAllByArticleId(int articleId)
        {
            return _tagsRepository.GetAllByArticleId(articleId);
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _tagsRepository.GetByIdAsync(id);
        }
    }
}
