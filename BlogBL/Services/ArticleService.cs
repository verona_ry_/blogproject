﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;
using BlogDAL.Repositories;

namespace BlogBL.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticleService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public async Task<BlogsArticle> CreateAsync(BlogsArticle article)
        {
            if (!IsTitleValid(article.Title))
            {
                throw new ArgumentException();
            }
            article.TextPreview = ChopString(article);

            return await _articleRepository.CreateAsync(article);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _articleRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<BlogsArticle>> GetAllAsync()
        {
            return await _articleRepository.GetAllAsync();
        }

        public IEnumerable<BlogsArticle> GetAllForOneUserAsync(int userId)
        {
            return _articleRepository.GetAllForOneUserAsync(userId);
        }

        public IEnumerable<BlogsArticle> GetAllByTagName(string tagName)
        {
            return _articleRepository.GetAllByTagName(tagName);
        }

        public async Task<BlogsArticle> GetByIdAsync(int id)
        {
            return await _articleRepository.GetByIdAsync(id);
        }

        public async Task<bool> UpdateAsync(int id,BlogsArticle article)
        {
            var existingArticle = await GetByIdAsync(id);
            existingArticle.TextPreview = ChopString(article);
            existingArticle.Text = article.Text;
            existingArticle.Category = article.Category;
            existingArticle.Title = article.Title;

            return await _articleRepository.UpdateAsync(existingArticle);
        }

        private bool IsTitleValid(string title)
        {
            return title.Length > 0 && char.IsLetter(title[0]) && char.IsUpper(title[0]);
        }

        private string ChopString(BlogsArticle article)
        {
            if (String.IsNullOrEmpty(article.Text))
                throw new ArgumentNullException(article.Text);
            if(article.Text.Length < 500)
            {
                return article.Text;
            }
            var words = article.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (words[0].Length > 500)
                return words[0];
            var sb = new StringBuilder();

            foreach (var word in words)
            {
                if ((sb + word).Length > 500)
                    return string.Format("{0}...", sb.ToString().TrimEnd(' '));
                sb.Append(word + " ");
            }
            return string.Format("{0}...", sb.ToString().TrimEnd(' '));
        }
    }
}
