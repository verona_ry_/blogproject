﻿using BlogDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Repositories;

namespace BlogBL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<BlogUser> CreateUserAsync(BlogUser user)
        {
            return await _userRepository.CreateUserAsync(user);
        }

        public async Task<IEnumerable<BlogUser>> GetAllUsersAsync()
        {
            return await _userRepository.GetAllUsersAsync();
        }

        public BlogUser GetUserByUserId(int userId)
        {
            return _userRepository.GetUserByUserId(userId);
        }

        public async Task<BlogUser> GetUserByUserName(string username)
        {
            return await _userRepository.GetUserByUserName(username);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _userRepository.DeleteAsync(id);
        }
    }
}
