﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using BlogDAL.Repositories;
using BlogBL.Services;

namespace BlogProject
{
    public class DependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArticleRepository>().To<ArticleRepository>();
            Bind<IArticleService>().To<ArticleService>();

            Bind<IUserRepository>().To<UserRepository>();
            Bind<IUserService>().To<UserService>();

            Bind<ICommentRepository>().To<CommentRepository>();
            Bind<ICommentService>().To<CommentService>();

            Bind<ITagsRepository>().To<TagsRepository>();   
            Bind<ITagService>().To<TagService>();
        }
    }
}