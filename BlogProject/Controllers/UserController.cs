﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BlogBL.Services;
using BlogDAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;


namespace BlogProject.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize(Roles ="Admin")]
        public async Task<ActionResult> Index()
        {            
            return View(await _userService.GetAllUsersAsync());
        }

        public ActionResult Details(int id)
        {
            return View(_userService.GetUserByUserId(id));
        }

        public string GetUserNameByUserId(int userId)
        {
            var user = _userService.GetUserByUserId(userId);
            return user.Username.ToString();
        }

        public ActionResult SignInOrLogIn()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin"))
                {
                    return Redirect("AdminPage");
                }
                return Redirect("UserPage");
            }
            return View();
        }

        public ActionResult Create() 
        { 
            return View(); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BlogUser blogUser)
        {
            try
            {
                
                await _userService.CreateUserAsync(blogUser);
                LoginInfo loginInfoNew = new LoginInfo
                {
                    Username = blogUser.Username,
                    Password = blogUser.Password,
                };
                return RedirectToAction("Login", new {loginInfo = loginInfoNew});
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            return View(); 
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            return Redirect("UserPage");
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginInfo loginInfo)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("Login");
            }
            
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);
            if(manager.Find(loginInfo.Username, loginInfo.Password) == null)
            {
                return Redirect("Login");
            }
            var user = manager.Find(loginInfo.Username, loginInfo.Password);
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);
            return RedirectToAction("UserPage");            
        }
        
        [Authorize]
        public async Task<ActionResult> UserPage()
        {
            try
            {
                if (User.IsInRole("Admin"))
                {
                    return Redirect("AdminPage");
                }
                var userModel = await _userService.GetUserByUserName(User.Identity.Name);

                ViewBag.Id = userModel.Id;
                ViewBag.Name = User.Identity.Name;
                return View();
            }
            catch
            {
                return View("Login");
            }
        }

        [Authorize(Roles ="Admin")]
        public ActionResult AdminPage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();

            return RedirectToAction("Index","BlogsArticles");
        }

        public async Task<ActionResult> CreateArticle()
        {
            var userModel = await _userService.GetUserByUserName(User.Identity.Name);
            return RedirectToAction("Create", "BlogsArticles", new { id = userModel.Id });
        }

        public async Task<ActionResult> ShowAllUsersArticles()
        {
            var userModel = await _userService.GetUserByUserName(User.Identity.Name);
            return RedirectToAction("ShowAllForOneUser", "BlogsArticles", new { userId = userModel.Id });

        }

        public ActionResult Delete(int id)
        {
            return View(_userService.GetUserByUserId(id));
        }

        // POST: BlogUser/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, BlogUser blogUser)
        {
            try
            {                
                var userStore = new UserStore<IdentityUser>();
                var manager = new UserManager<IdentityUser>(userStore);
                var userToDelete = manager.FindByName(_userService.GetUserByUserId(id).Username);
                await manager.DeleteAsync(userToDelete);
                await _userService.DeleteAsync(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}                          