﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BlogBL.Services;
using BlogDAL;
using BlogDAL.Models;
using PagedList;
using PagedList.Mvc;


namespace BlogProject.Controllers
{
    public class BlogCommentsController : Controller
    {
        private ICommentService _commentService;

        public BlogCommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            return View(await _commentService.GetAllAsync());
        }

        public async Task<ActionResult> Details(int id)
        {
            return View(await _commentService.GetByIdAsync(id));
        }

        [Authorize(Roles = "User")]
        // GET: BlogComments/Create
        public ActionResult Create(int articleId)
        {
            ViewBag.articleId = articleId;
            return View();
        }

        // POST: BlogComments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CommentText,Created,ArticleId")] BlogComments comment)
        {
            if (ModelState.IsValid)
            {
                int id = Convert.ToInt32(Request["articleId"]);
                comment.ArticleId = id;
                comment.CommentCreatedTime = DateTime.Now;
                comment.UserName = User.Identity.Name;
                _commentService.CreateAsync(comment);
                return RedirectToAction("Details", "BlogsArticles", new {id = id});
            }

            return View(comment);
        }

        // GET: BlogComments/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            return View(await _commentService.GetByIdAsync(id));
        }

        // POST: BlogComments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, BlogComments comment)
        {
            try
            {
                comment.Id = id;
                await _commentService.UpdateAsync(comment);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BlogComments/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            return View(await _commentService.GetByIdAsync(id));
        }

        // POST: BlogComments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        ///
        public async Task<ActionResult> Delete(int id, BlogComments comment) 
        {
            try
            {
                await _commentService.DeleteAsync(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult ViewArticleComments(int id, int? page)
        {
            return PartialView(_commentService.GetAllByArticleIdAsync(id).ToList());
        }
    }
}
