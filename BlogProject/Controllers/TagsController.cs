﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogDAL;
using BlogDAL.Models;
using BlogBL.Services;
using System.Threading.Tasks;

namespace BlogProject.Controllers
{
    public class TagsController : Controller
    {
        private ITagService _tagService;

        public TagsController(ITagService tagService)
        {
            _tagService = tagService;
        }
                
        public PartialViewResult ViewArticleTags(int id)                                                                                     
        {
            return PartialView(_tagService.GetAllByArticleId(id));
        }
    }
}
