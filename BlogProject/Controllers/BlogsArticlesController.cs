﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogDAL;
using BlogDAL.Models;
using BlogDAL.Repositories;
using BlogBL.Services;
using System.Threading.Tasks;
using PagedList;
using PagedList.Mvc;

namespace BlogProject.Controllers
{
    public class BlogsArticlesController : Controller
    {
        private IArticleService _articleService;
        private ITagService _tagService;

        public BlogsArticlesController(IArticleService articleService, ITagService tagService)
        {
            _articleService = articleService;
            _tagService = tagService;
        }

        // GET: BlogsArticles
        public async Task<ActionResult> Index(int? page)
        {
            var articles = await _articleService.GetAllAsync();
            return View(articles.ToList().ToPagedList(page ?? 1, 9));
        }

        [HttpPost]                                                                          
        public async Task<ActionResult> Index(string SearchString, int? page)
        {
            var articles = await _articleService.GetAllAsync();

            string searchParameter = Request["searchParameter"];
            if (!String.IsNullOrEmpty(SearchString))
            {
                if(searchParameter == "title")
                {
                    articles = articles.Where(s => s.Title.Contains(SearchString));
                }
                else
                {
                    articles = _articleService.GetAllByTagName(SearchString);
                }
                
            }
            return View(articles.ToList().ToPagedList(page ?? 1, 3));
        }

        public PartialViewResult ShowAllForOneUser(int userId)
        {
            return PartialView(_articleService.GetAllForOneUserAsync(userId));
        }

        // GET: BlogsArticles/Details/5
        public async Task<ActionResult> Details(int id)
        {
            return View(await _articleService.GetByIdAsync(id));
        }

        // GET: BlogsArticles/Create
        public ActionResult Create(int id)
        {
            ViewBag.Id = id.ToString();
            return View();
        }

        [Authorize(Roles = "User")]
        public ActionResult CreateComment(int id)
        {
            return RedirectToAction("Create", "BlogComments", new {articleId = id});
        }
        // POST: BlogsArticles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Text,Created,Category,UserId")] BlogsArticle article)
        {
            if (ModelState.IsValid)
            {                
                int id = Convert.ToInt32(Request["UserId"]);
                article.UserId = id;
                article.Created = DateTime.Now;
                string tagsInput = Request["tagsInput"];
                article.tags = _tagService.CreateAsync(tagsInput).Result.ToList();
                await _articleService.CreateAsync(article);
                return RedirectToAction("Index");
            }

            return View(article);
        }

        // GET: BlogsArticles/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            return View(await _articleService.GetByIdAsync(id));
        }

        // POST: BlogsArticles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, BlogsArticle article)
        {
            try
            {
                article.Id = id;
                await _articleService.UpdateAsync(id,article);

                return RedirectToAction("UserPage","User");
            }
            catch
            {
                return View();
            }
        }

        // GET: BlogsArticles/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            return View(await _articleService.GetByIdAsync(id));
        }

        // POST: BlogsArticles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, BlogsArticle article)
        {
            try
            {
                await _articleService.DeleteAsync(id);
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("AdminArticleView");
                }
                return RedirectToAction("UserPage", "User");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult ViewArticleTags(int id)
        {
            return PartialView(_tagService.GetAllByArticleId(id));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AdminArticleView()
        {
            return View(await _articleService.GetAllAsync());
        }

    }
}
