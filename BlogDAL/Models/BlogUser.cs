﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BlogUser : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public ICollection<BlogsArticle> blogsArticles { get; set; }
        //public UserRole Role { get; set; }
    }

   /* public enum UserRole
    {
        RegisteredUser,
        Admin
    }*/
}
