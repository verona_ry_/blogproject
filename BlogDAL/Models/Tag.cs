﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Tag : BaseEntity
    {
        public string TagName { get; set; }
        public ICollection<BlogsArticle> Articles { get; set; }

        public Tag()
        {
            Articles = new List<BlogsArticle>();
        }
    }
}
