﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginInfo : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
