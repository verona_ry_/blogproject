﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BlogsArticle : BaseEntity
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string TextPreview { get; set; }
        public DateTime Created { get; set; }
        public string Category { get; set; }
        public int UserId { get; set; }
        public BlogUser BlogUser { get; set; }
        public ICollection<BlogComments> blogsComments { get; set; }
        public ICollection<Tag> tags { get; set; }
    }
}
