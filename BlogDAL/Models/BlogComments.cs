﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BlogComments : BaseEntity
    {
        public string CommentText { get; set; }
        public string UserName { get; set; }
        public int ArticleId { get; set; }
        public BlogsArticle Article { get; set; }
        public DateTime CommentCreatedTime { get; set; }
    }
}
