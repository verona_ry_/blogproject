﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL
{
    public class BlogContext : DbContext
    {
        public DbSet<BlogsArticle> blogsArticles { get; set; }
        public DbSet<BlogUser> blogUsers { get; set; }
        public DbSet<BlogComments> blogComments { get; set; }
        public DbSet<Tag> blogTags { get; set; }
        public System.Data.Entity.DbSet<BlogDAL.Models.LoginInfo> LoginInfoes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogsArticle>()
                .HasRequired<BlogUser>(s => s.BlogUser)
                .WithMany(g => g.blogsArticles)
                .HasForeignKey<int>(s=>s.UserId);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<BlogComments>()
                .HasRequired<BlogsArticle>(s => s.Article)
                .WithMany(g => g.blogsComments)
                .HasForeignKey<int>(s => s.ArticleId);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }        
    }
}
