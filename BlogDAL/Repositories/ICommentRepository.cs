﻿using BlogDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Repositories
{
    public interface ICommentRepository
    {
        Task<BlogComments> CreateAsync(BlogComments comment);
        Task<IEnumerable<BlogComments>> GetAllAsync();
        Task<BlogComments> GetByIdAsync(int id);
        Task<bool> UpdateAsync(BlogComments comment);
        Task<bool> DeleteAsync(int id);
        IEnumerable<BlogComments> GetAllByArticleIdAsync(int id);
    }
}
