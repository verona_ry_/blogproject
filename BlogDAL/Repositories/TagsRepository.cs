﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Repositories
{
    public class TagsRepository : ITagsRepository
    {
        private readonly BlogContext _blogContext;

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="blogContext">BlogContext entity.</param>
        public TagsRepository(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        /// <summary>
        /// Create a new article.
        /// </summary>
        /// <param name="tagName">Name of the new tag.</param>
        /// <returns>A newly created tag.</returns>
        public async Task<Tag> Create(string tagName)
        {
            Tag tag = new Tag { TagName = tagName };
            _blogContext.blogTags.Add(tag);
            await _blogContext.SaveChangesAsync();
            return tag;
        }

        /// <summary>
        /// Create a list of tags from the input string of tags' names.
        /// </summary>
        /// <param name="tagsNames">A string with tags names.</param>
        /// <returns>A list of tags.</returns>
        public async Task<IEnumerable<Tag>> CreateTagListAsync(string tagsNames)
        {
            var resultList = new List<Tag>();
            if (tagsNames.Length > 0)
            {
                List<string> tagsNamesList = tagsNames.Split('#').ToList();
                foreach (string tagName in tagsNamesList)
                {
                    if(tagName.Length > 0 && tagName != " ")
                    {
                        if (!_blogContext.blogTags.Any(x => x.TagName == tagName))
                        {
                            await Create(tagName);
                        }
                        resultList.Add(_blogContext.blogTags.Where(y => y.TagName == tagName).FirstOrDefault());
                    }                    
                }
                await _blogContext.SaveChangesAsync();
            }                      
            return resultList;
        }

        /// <summary>
        /// Delete a tag by id.
        /// </summary>
        /// <param name="id">Id of the tag to delete.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _blogContext.blogTags.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _blogContext.Entry(item).State = EntityState.Modified;
                _blogContext.blogTags.Remove(item);
                await _blogContext.SaveChangesAsync();
            }
            return result;
        }

        /// <summary>
        /// Get a list of all articles.
        /// </summary>
        /// <returns>A list of existing tags.</returns>
        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await _blogContext.blogTags.ToListAsync();
        }

        /// <summary>
        /// Get a list of all tags for one articles.
        /// </summary>
        /// <param name="articleId">Id of the article.</param>
        /// <returns>A list of tags for one article.</returns>
        public IEnumerable<Tag> GetAllByArticleId(int articleId)
        {
            var tags = _blogContext.blogTags.Where(x => x.Articles.Any(y => y.Id == articleId)).ToList();
            if(tags == null)
            {
                return new List<Tag>();
            }
            return tags;
        }

        /// <summary>
        /// Get a tag by its id.
        /// </summary>
        /// <param name="id">Id of the tag</param>
        /// <returns>A Tag entity with the specified id.</returns>
        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _blogContext.blogTags.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        
    }
}
