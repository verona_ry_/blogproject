﻿using BlogDAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly BlogContext _blogContext;

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="blogContext">BlogContext entity.</param>
        public CommentRepository(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        /// <summary>
        /// Create a new comment.
        /// </summary>
        /// <param name="comment">BlogComment entity.</param>
        /// <returns>A newly created comment.</returns>
        public async Task<BlogComments> CreateAsync(BlogComments comment)
        {
            _blogContext.blogComments.Add(comment);
            await _blogContext.SaveChangesAsync();
            return comment;
        }

        /// <summary>
        /// Delete a comment by id.
        /// </summary>
        /// <param name="id">Id of the comment to delete.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _blogContext.blogComments.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _blogContext.Entry(item).State = EntityState.Modified;
                _blogContext.blogComments.Remove(item);
                await _blogContext.SaveChangesAsync();
            }
            return result;
        }

        /// <summary>
        /// Get a list of all comments.
        /// </summary>
        /// <returns>A list of existing comments.</returns>
        public async Task<IEnumerable<BlogComments>> GetAllAsync()
        {
            return await _blogContext.blogComments.ToListAsync();
        }

        /// <summary>
        /// Get a list of comments for one article.
        /// </summary>
        /// <param name="id">Id of the article.</param>
        /// <returns>A list of comments for one article.</returns>
        public IEnumerable<BlogComments> GetAllByArticleIdAsync(int id)
        {
            return _blogContext.blogComments.Where(x => x.ArticleId == id).ToList();
        }

        /// <summary>
        /// Get a comment by its id.
        /// </summary>
        /// <param name="id">Id of the comment.</param>
        /// <returns>An BlogComment entity with the specified id.</returns>
        public async Task<BlogComments> GetByIdAsync(int id)
        {
            return await _blogContext.blogComments.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Update a comment.
        /// </summary>
        /// <param name="comment">BlogComment entity to update.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> UpdateAsync(BlogComments comment)
        {
            var item = _blogContext.blogComments.Attach(comment);
            _blogContext.Entry(comment).State = EntityState.Modified;
            await _blogContext.SaveChangesAsync();
            return item != null;
        }
    }
}
