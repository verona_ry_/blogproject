﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly BlogContext _blogContext;

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="blogContext">BlogContext entity.</param>
        public ArticleRepository(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        /// <summary>
        /// Create a new article.
        /// </summary>
        /// <param name="article">BlogsArticle entity.</param>
        /// <returns>A newly created article.</returns>
        public async Task<BlogsArticle> CreateAsync(BlogsArticle article)
        {
            _blogContext.blogsArticles.Add(article);
            await _blogContext.SaveChangesAsync();
            return article;
        }

        /// <summary>
        /// Delete an article by id.
        /// </summary>
        /// <param name="id">Id of the article to delete.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _blogContext.blogsArticles.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _blogContext.Entry(item).State = EntityState.Modified;
                _blogContext.blogsArticles.Remove(item);
                await _blogContext.SaveChangesAsync();
            }
            return result;
        }

        /// <summary>
        /// Get a list of all articles.
        /// </summary>
        /// <returns>A list of existing articles.</returns>
        public async Task<IEnumerable<BlogsArticle>> GetAllAsync()
        {
            return await _blogContext.blogsArticles.ToListAsync();
        }

        /// <summary>
        /// Get a list of articles written by one user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>A list of articles created by the user.</returns>
        public IEnumerable<BlogsArticle> GetAllForOneUserAsync(int userId)
        {
            return _blogContext.blogsArticles.Where(x => x.UserId == userId).ToList();
        }

        /// <summary>
        /// Get all articles by the specific tag they contain.
        /// </summary>
        /// <param name="tagName">Name of the tag articles need to contain.</param>
        /// <returns>A list of articles containing a specified tag.</returns>
        public IEnumerable<BlogsArticle> GetAllByTagName(string tagName)
        {
            return _blogContext.blogsArticles.Where(x => x.tags.Any(t => t.TagName == tagName)).ToList();
        }

        /// <summary>
        /// Get an article by its id.
        /// </summary>
        /// <param name="id">Id of the article.</param>
        /// <returns>An BlogsArticle entity with the specified id.</returns>
        public async Task<BlogsArticle> GetByIdAsync(int id)
        {
            return await _blogContext.blogsArticles.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Update an article.
        /// </summary>
        /// <param name="article">BlogsArticle entity to update.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> UpdateAsync(BlogsArticle article)
        {
            var item = _blogContext.blogsArticles.Attach(article);
            _blogContext.Entry(article).State = EntityState.Modified;
            await _blogContext.SaveChangesAsync();
            return item != null;
        }
    }
}
