﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Repositories
{
    public interface IArticleRepository
    {
        Task<BlogsArticle> CreateAsync(BlogsArticle article);
        Task<IEnumerable<BlogsArticle>> GetAllAsync();
        IEnumerable<BlogsArticle> GetAllForOneUserAsync(int userId);
        IEnumerable<BlogsArticle> GetAllByTagName(string tagName);
        Task<BlogsArticle> GetByIdAsync(int id);
        Task<bool> UpdateAsync(BlogsArticle article);
        Task<bool> DeleteAsync(int id);
    }
}
