﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Repositories
{
    public interface ITagsRepository
    {
        Task<Tag> Create(string tagName);
        Task<IEnumerable<Tag>> CreateTagListAsync(string tagsNames);
        Task<IEnumerable<Tag>> GetAllAsync();
        IEnumerable<Tag> GetAllByArticleId(int articleId);
        Task<Tag> GetByIdAsync(int id);
        Task<bool> DeleteAsync(int id);
    }
}
