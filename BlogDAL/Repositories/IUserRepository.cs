﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;

namespace BlogDAL.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<BlogUser>> GetAllUsersAsync();
        BlogUser GetUserByUserId(int userId);
        Task<BlogUser> GetUserByUserName(string username);
        Task<BlogUser> CreateUserAsync(BlogUser user);
        Task CreateUserRolesAsync();
        Task<bool> DeleteAsync(int id);
    }
}
