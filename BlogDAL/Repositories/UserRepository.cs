﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogDAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BlogDAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly BlogContext _blogContext;

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="blogContext">BlogContext entity.</param>
        public UserRepository(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user">BlogUser entity.</param>
        /// <returns>A newly created user.</returns>
        public async Task<BlogUser> CreateUserAsync(BlogUser user)
        {
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            var _user = new IdentityUser() { UserName = user.Username };
            IdentityResult result = await manager.CreateAsync(_user, user.Password);
            _blogContext.blogUsers.Add(user);
            await manager.AddToRoleAsync(_user.Id, "User");
            await _blogContext.SaveChangesAsync();

            return user;
        }

        /// <summary>
        /// Create two user roles:
        /// - Admin (an administrator of the blog website);
        /// - User (a regulal registered website user).
        /// </summary>
        public async Task CreateUserRolesAsync()
        {
            var roleStore = new RoleStore<IdentityRole>();
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var identityResult = await roleManager.CreateAsync(new IdentityRole
            {
                Name = "Admin"
            });
            var identityResult2 = await roleManager.CreateAsync(new IdentityRole
            {
                Name = "User"
            });

            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = await userManager.FindAsync("AdminUser", "AdminUser");
            await userManager.AddToRoleAsync(user.Id, "Admin");
        }

        /// <summary>
        /// Get a list of all users.
        /// </summary>
        /// <returns>A list of existing users.</returns>
        public async Task<IEnumerable<BlogUser>> GetAllUsersAsync()
        {
            return await _blogContext.blogUsers.ToListAsync();
        }

        /// <summary>
        /// Get a user by its id.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        /// <returns>An BlogUser entity with the specified id.</returns>
        public BlogUser GetUserByUserId(int userId)
        {
            return _blogContext.blogUsers.Find(userId);
        }

        /// <summary>
        /// Get a user by its username.
        /// </summary>
        /// <param name="username">User's username.</param>
        /// <returns>A user with the specified username.</returns>
        public async Task<BlogUser> GetUserByUserName(string username)
        {
            return await _blogContext.blogUsers.Where(u => u.Username == username).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Delete a user by id.
        /// </summary>
        /// <param name="id">Id of the user to delete.</param>
        /// <returns>"true" if the action was successful, "false" - if not.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _blogContext.blogUsers.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _blogContext.Entry(item).State = EntityState.Modified;
                _blogContext.blogUsers.Remove(item);
                await _blogContext.SaveChangesAsync();
            }
            return result;
        }
    }
}
