﻿namespace BlogDAL.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using BlogDAL.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<BlogDAL.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
            ContextKey = "BlogDAL.BlogContext";
        }

        protected override void Seed(BlogDAL.BlogContext context)
        {
            /*var articles = new List<BlogsArticle>
            {
                new BlogsArticle{ Title = "New Article", Category = "Food", Created = DateTime.Now, Text="Some text", UserId = 1 }
            };*/
        }
    }
}
