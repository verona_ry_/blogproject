﻿namespace BlogDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlogsArticle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        TextPreview = c.String(),
                        Created = c.DateTime(nullable: false),
                        Category = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BlogUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.BlogArticle");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BlogArticle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        articleTitle = c.String(nullable: false, maxLength: 40),
                        articleText = c.String(nullable: false),
                        articleTextPreview = c.String(),
                        Created = c.DateTime(nullable: false),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.BlogUser");
            DropTable("dbo.BlogsArticle");
        }
    }
}
